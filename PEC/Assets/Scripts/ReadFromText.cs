﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ReadFromText : MonoBehaviour {

    public Text introText;
    public float letterPause = 0.05f;
    public LevelChanger transition;
    public GameObject button;

    private string message = "";
    private List<string> introduction = new List<string>();
    private int numChars = 0;
    private string[] fileLines;
   

    private void Start()
    {
        //string temp = GameplayManager.LoadResourceTextfile("Introduccion");
        button.gameObject.SetActive(false);
        string temp = "Tras días preguntando, por fin has llegado al Scumm Bar... Una taberna" +
                    " de mala muerte con gente de la peor calaña. Justo lo que necesitas para esta aventura." +
                    "\n\n" +
                    "Sabes que no será fácil, pero necesitas una tripulación que te siga y " +
                    "esté lo suficientemente loca." +
                    "\n\n" +
                    " Decidido a ello, te dispones a entrar.";
        fileLines = temp.Split('\n');
        
        for (int i = 0; i < fileLines.Length; i++)
        {
            message += "\n" + fileLines[i];
        }

        StartCoroutine(TypeText());

        introText.text = "";
    }
    
    
    IEnumerator TypeText()
    {
        foreach (char letter in message)
        {
            introText.text += letter;
            numChars++;
            if (numChars == message.Length)
            {
                button.gameObject.SetActive(true);
            }
                
            yield return new WaitForSeconds(letterPause);
        }
    }

    private void Update()
    {
        if (Input.anyKey)
            letterPause = 0f;
    }
}
