﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour {

    public Animator animator;
    public AudioSource audioSource;

    private int levelToLoad;
    
    public void FadeToLevel (int levelIndex)
    {
        levelToLoad = levelIndex;
        animator.SetTrigger("EndScene");
        StartCoroutine(AudioFadeOut(audioSource, 1.0f));

    }

    public void OnFadeComplete()
    {
        SceneManager.LoadScene(levelToLoad);
    }

    public IEnumerator AudioFadeOut (AudioSource audio, float fadeTime)
    {
        float startVolume = audio.volume;

        while (audio.volume > 0)
        {
            audio.volume -= startVolume * Time.deltaTime / fadeTime;
            yield return null;
        }

        audio.Stop();
        audio.volume = startVolume;
    }

}
