﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Lista de ataques y respuesta del jugador y la IA.
/// </summary>
/// <param name="ataques">Array de posibles ataques.</param>
/// <param name="replicas">Array de posibles respuestas.</param>
[Serializable]
public class Insults
{
    public string[] ataques = new string[] { };
    public string[] replicas = new string[] { };

}

public class GameplayManager : MonoBehaviour {

    public class StoryNode {
        /// <summary>
        /// El texto que describe la localización.
        /// </summary>
        public string history;
        /// <summary>
        /// La lista de textos con las respuestas posibles.
        /// </summary>
        public string[] answers;
        /// <summary>
        ///  La lista de nodos destino, en el mismo orden que en la lista
        ///  de respuestas. O sea, si se elige la respuesta en el índice 1 de «answers», el 
        ///  jugador se desplazará al nodo indicado por nextNode[1].
        /// </summary>
        public StoryNode[] nextNode;
        /// <summary>
        /// Indica que este nodo corresponde a un final, donde acaba la
        /// aventura.
        /// </summary>
        public bool isFinal = false;

        public bool newRound = false;

        public delegate void NodeVisited(int index);
        /// <summary>
        ///  Permite asignar una referencia a un método
        ///  delegado que se ejecutará siempre que se visite esa localización.Esta
        ///  estrategia permite ejecutar código que manipule el mapa al llegar a ciertas
        ///  localizaciones.
        /// </summary>
        public NodeVisited nodeVisited;
    }



    public Text historyText;
    public Transform answersParent;
    public GameObject buttonAnswerPrefab;
    public Scrollbar answerScrollbar;
    public float letterPause = 0.05f;
    public GameObject lifeBarPrefab;
    public Transform historyParent;
    
    public static float difficulty; //% de acierto base de la IA
    public static int enemyScore;
    public static int playerScore;
    public static int maxScore = 3;

    /// <summary>
    ///  indicará en todo momento en qué nodo se encuentra actualmente el jugador.
    /// </summary>
    private StoryNode current;

    private int randomIndex;
    private string message;
    private bool messageFinished;
    private int numChars;
    private int selectedAnswer;
    private bool isWaiting;
    private Image[] playerHealth;
    private Image[] enemyHealth;
    private bool healthCreated = false;
    private GameObject playerHealthBar;
    private GameObject enemyHealthBar;


    private void Start()
    {
        Destroy(playerHealthBar);
        Destroy(enemyHealthBar);
        playerScore = 0;
        enemyScore = 0;
        difficulty = 0;
        healthCreated = false;
        current = StoryFiller.FillStory();
        historyText.text = "";
        FillUI();
    }
    
    void FillUI()
    {
        if (!healthCreated && difficulty > 0)
            CreateLifeBars();

        isWaiting = false;
        letterPause = 0.05f;

        // Añadimos un par de lineas en blanco y el texto corresponiendte al nodo actual
        if (current.newRound) 
            historyText.text = "";
        else
            historyText.text += "\n";

        message = "\n" + current.history;
        numChars = 0;
        StartCoroutine(TypeText());

        // Borramos los anteriores botones
        foreach (Transform child in answersParent.transform)
        {
            Destroy(child.gameObject);
        }

        float height = 280f;
        int index = 0;
        // Colocamos los nuevos botones
        // Por cada respuesta crearemos un botón
        foreach (string answer in current.answers)
        {
            
            // Creamos el botón y lo asociamos al enemento de la UI correspondiente.
            GameObject buttonAnswerCopy = Instantiate(buttonAnswerPrefab);
            buttonAnswerCopy.transform.parent = answersParent;

            // Le damos la posición adecuada según pertoque
            buttonAnswerCopy.GetComponent<RectTransform>().localPosition = new Vector3(0, height, 0);
            
            // Sumamos distancia para colocarlos en la siguiente línea
            height += buttonAnswerCopy.GetComponent<RectTransform>().rect.y * 2.0f;

            // Asociamos
            FillListener(buttonAnswerCopy.GetComponent<Button>(), index);

            // Rellenamos el texto del botón
            buttonAnswerCopy.GetComponentInChildren<Text>().text = answer;
 
            index++;
        }
    }


    IEnumerator TypeText()
    {
        foreach (char letter in message)
        {
            historyText.text += letter;
            numChars++;
            if (numChars == message.Length)
                messageFinished = true;
            else
                messageFinished = false;
            yield return new WaitForSeconds(letterPause);
        }
    }


    /// <summary>
    /// Esta función asocia el hacer click en el botón, con la función 'AnswerSelected'.
    /// </summary>
    /// <param name="button">Botón sobre el que se añadirá la acción.</param>
    /// <param name="index">posición del botón dentro de la lista de respuestas.</param>
    void FillListener(Button button, int index)
    {
        button.onClick.AddListener(
            () => {
                AnswerSelected(index);
            }
            );
    }

    /// <summary>
    /// Esta es la función a la que se llamará cuando se toque el botón.
    /// </summary>
    /// <param name="index">posición del botón dentro de la lista de respuestas.</param>
    void AnswerSelected(int index)
    {
        Debug.Log(difficulty);
        // Añadimos la respuesta seleccionada a la historia
        if (!messageFinished)
        {
            letterPause = 0f;
            selectedAnswer = index;
            isWaiting = true;
            return;
        }

        historyText.text += "\n\t" + current.answers[index];  

        // Si no estamos en el nodo final
        if (!current.isFinal)
        {
            //Generamos el siguiente nodo de ataque, aleatorio, de la IA
            randomIndex = UnityEngine.Random.Range(0, StoryFiller.insultList.ataques.Length);

            // Si este nodo tenía alguna acción asociada, la ejecutamos.
            if (current.nodeVisited != null)
                current.nodeVisited(randomIndex);   //Nuevo ataque, para no repetir la estrategia de la IA.

            // Asociamos el nodo siguiente como el actual
            current = current.nextNode[index];

            UpdateHealth();
            // Rellenamos toda la UI
            FillUI();
        }
        else
        {
            // Final de la Partida
            historyText.text += "\n" + "TOCA ESCAPE PARA CONTINUAR";
        }
        answerScrollbar.value = 1.0f;
    }

    /// <summary>
    /// Esta funcion comprueba si la IA acierta o falla ante nuestro insulto.
    /// </summary>
    public bool ComprobeCorrectAnswer()
    {
        bool isCorrect;
        float rnd = UnityEngine.Random.value;
        if (rnd <= difficulty)
            isCorrect = true;
        else
            isCorrect = false;

        return isCorrect;
    }

    private void Update()
    {
        if (isWaiting && messageFinished && !current.isFinal)
            AnswerSelected(selectedAnswer);

        if (Input.anyKey && !messageFinished)
            letterPause = 0f;
    }
    
    void CreateLifeBars()
    {
        playerHealthBar = Instantiate(lifeBarPrefab);
        enemyHealthBar = Instantiate(lifeBarPrefab);

        playerHealthBar.transform.parent = enemyHealthBar.transform.parent = historyParent;
        
        // Le damos la posición adecuada según pertoque
        playerHealthBar.GetComponent<RectTransform>().localPosition = new Vector3(-300, -105, 0);
        enemyHealthBar.GetComponent<RectTransform>().localPosition = new Vector3(300, -105, 0);

        // Rellenamos el texto del botón
        playerHealthBar.GetComponentInChildren<Text>().text = "Jugador: ";
        enemyHealthBar.GetComponentInChildren<Text>().text = "Enemigo: ";

        //Asociamos las imagenes de la vida
        playerHealth = playerHealthBar.GetComponentsInChildren<Image>();
        enemyHealth = enemyHealthBar.GetComponentsInChildren<Image>();

        healthCreated = true;
    }
    

    void UpdateHealth()
    {
        for (int i = 1; i <= maxScore; i++)
        {
            if (playerScore == i)
            {
                enemyHealth[i].GetComponent<Image>().enabled = false;
            }

            if (enemyScore == i)
            {
                playerHealth[i].GetComponent<Image>().enabled = false;
            }
        }
    }

    public static string LoadResourceTextfile(string path)
    {
        string filePath = "Text/" + path;
        
        TextAsset targetFile = Resources.Load<TextAsset>(filePath);

        if (targetFile == null)
            Debug.Log("targetfile not found");

        return targetFile.text;
    }
}
