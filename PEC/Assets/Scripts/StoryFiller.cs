﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class StoryFiller
{
    private static string json = @"{
            ""ataques"": [
              ""¡Llevarás mi espada como si fueras un pincho moruno!"",
              ""¡Luchas como un granjero!"",
              ""¡No hay palabras para describir lo asqueroso que eres!"",
              ""¡He hablado con simios más educados que tú!"",
              ""¡No pienso aguantar tu insolencia aquí sentado!"",
              ""¡Mi pañuelo limpiará tu sangre!"",
              ""¡Ha llegado tu HORA, palurdo de ocho patas!"",
              ""¿Has dejado de usar pañales?"",
              ""¡Una vez tuve un perro más listo que tú!"",
              ""¡Nadie me ha sacado sangre jamás, y nadie lo hará!"",
              ""¡Me das ganas de vomitar!"",
              ""¡Tienes los modales de un mendigo!"",
              ""¡He oído que eres un soplón despreciable!"",
              ""¡La gente cae a mis pies al verme llegar!"",
              ""¡Demasiado bobo para mi nivel de inteligencia!"",
              ""¡Obtuve esta cicratiz en mi cara en una lucha a muerte!""
              ],
              ""replicas"": [
                ""Primero deberías dejar de usarla como un plumero."",
                ""Que apropiado, tú peleas como una vaca."",
                ""Sí que las hay, sólo que nunca las has aprendido."",
                ""Me alegra que asistieras a tu reunión familiar diaria."",
                ""Ya te están fastidiando otra vez las almorranas, ¿Eh?"",
                ""Ah, ¿Ya has obtenido ese trabajo de barrendero?"",
                ""Y yo tengo un SALUDO para ti, ¿Te enteras?"",
                ""¿Por qué? ¿Acaso querías pedir uno prestado?"",
                ""Te habrá enseñado todo lo que sabes."",
                ""¿TAN rápido corres?"",
                ""Me haces pensar que alguien ya lo ha hecho."",
                ""Quería asegurarme de que estuvieras a gusto conmigo."",
                ""Qúe pena que nadie haya oído hablar de ti."",
                ""¿Incluso antes de que huelan tu aliento?"",
                ""Estaría acabado si la usases alguna vez."",
                ""Espero que ya hayas aprendido a no tocarte la nariz."",
                ""Yo soy cola, tú pegamento..."",
                ""Tú más..."",
                ""¿Ah sí?""
                 ]
               }";

    //static string json = GameplayManager.LoadResourceTextfile("Dialog");
    public static Insults insultList = JsonUtility.FromJson<Insults>(json);


    /// <summary>
    /// Función para crear un nuevo nodo a partir de un texto y unas respuestas.
    /// </summary>
    /// <param name="history">Texto que se mostrará al llegar al nodo.</param>
    /// <param name="options">Respuestas que aparecerán para reaccionar al texto del nodo.</param>
    private static GameplayManager.StoryNode CreateNode(string history, string[] options)
    {
        GameplayManager.StoryNode node = new GameplayManager.StoryNode();
        node.history = history;
        node.answers = options;
        node.nextNode = new GameplayManager.StoryNode[options.Length];
        return node;
    }

    /// <summary>
    /// Función para rellenar los nodos que forman el esqueleto principal de la historia.
    /// </summary>
    public static GameplayManager.StoryNode FillStory()
    {
        int rangoAtaques = insultList.ataques.Length;
        int rangoReplicas = insultList.replicas.Length;
        StoryFiller sf = new StoryFiller();
        GameplayManager gm = new GameplayManager();

        //----------------------------CREACION DE LOS NODOS-----------------------------
        GameplayManager.StoryNode root = CreateNode("El gran tumulto que se oía al entrar cesa durante un segundo mientras todas las miradas se dirigen a ti, sólo para volver a mezclarse en un ruido ambiental."
            ,new string[] { "Te dirijes a la barra.", "Gritas en busca de bucaneros dispuestos a unirse." });

        GameplayManager.StoryNode pacificRouteIntro = CreateNode("Antes de poder siquiera dirigirte al camarero, un tipo curtido con voz tosca y una vieja cicatriz que le cruza la cara te sujeta y te habla: \"¿Qué es lo que buscas aquí, muchacho?\""
            , new string[] { "Busco gente capaz de embarcarse en una gran aventura." });

        GameplayManager.StoryNode pacificRouteOutro = CreateNode("\"Chico... Para ser capitán hace falta algo más que ganas y habilidad con la espada... y te lo voy a demostrar. \""
            , new string[] { "Adelante... Sufrirás las consecuencias..." });

        GameplayManager.StoryNode aggressiveRouteIntro = CreateNode("Rápidamente un viejo de barba negruzca y con parche en el ojo se encara a ti: \"¡Muchacho, no tienes lo que hay que tener!\""
           , new string[] { "Te demostraré lo que soy capaz de hacer con mi espada, viejo." });

        GameplayManager.StoryNode aggressiveRouteOutro = CreateNode("\"Un capitán necesita mucho más que ser hábil con la espada... Necesita tener una lengua viperina más afilada que el acero... Ahora verás a lo que me refiero...\""
           , new string[] { "Vamos a verlo." });

        //Array de posibles turnos que puede llevar la computadora. Las respuestas para el usuario son siempre las mismas.
        GameplayManager.StoryNode[] enemyTurn = new GameplayManager.StoryNode[rangoAtaques]; 
        for (int i = 0; i < enemyTurn.Length; i++)
        {
            enemyTurn[i] = CreateNode(insultList.ataques[i], insultList.replicas);
            enemyTurn[i].newRound = true;
        }

        //Turno del jugador. Proporciona los posibles ataques.
        GameplayManager.StoryNode playerTurn = CreateNode("Tu rival se mantiene a la espera tras esa humillación.", insultList.ataques);
        playerTurn.newRound = true;
          //Nodo PC gana el turno.
        GameplayManager.StoryNode nodoPlayerCorrect = CreateNode(@"No está mal chico... Veamos que más tienes...",
              new string[] {"Te vas a enterar"});

          //Nodo NPC gana el turno
        GameplayManager.StoryNode nodoEnemyCorrect = CreateNode(@"¿Eso es todo lo que tienes, chaval?",
             new string[] {"Esta vez no te será tan fácil"});

          //Nodo final - Gana Jugador
        GameplayManager.StoryNode nodoFinPlayer = CreateNode(@"¡¡Me rindo chico!! Sin duda tienes madera para capitán",
            new string[] {"Salir del juego"});

          //Nodo final - Gana NPC
        GameplayManager.StoryNode nodoFinEnemy = CreateNode(
              @"Jajajaja, aún te queda mucho por aprender zagal. Vuelve cuando estés preparado de verdad",
              new string[] { "Salir del juego" });

        GameplayManager.StoryNode[] enemyAnswerCorrect = new GameplayManager.StoryNode[rangoAtaques];
        for (int i = 0; i < rangoAtaques; i++)
        {
            enemyAnswerCorrect[i] = CreateNode("", new string[] { "Te sientes terriblemente humillado" });
        }
        
        GameplayManager.StoryNode enemyAnswerIncorrect = CreateNode("", new string[] { "Tu enemigo se siente extremadamente estupido" });


        //--------------- ASOCIACIÓN DE UNOS NODOS CON OTROS ----------------------//
        root.newRound = true;
        root.nextNode[0] = pacificRouteIntro;
        root.nextNode[1] = aggressiveRouteIntro;

        pacificRouteIntro.nextNode[0] = pacificRouteOutro;
        aggressiveRouteIntro.nextNode[0] = aggressiveRouteOutro;

        pacificRouteOutro.nodeVisited = (x) =>
        {
            GameplayManager.difficulty = 0.6f;
            pacificRouteOutro.nextNode[0] = enemyTurn[x];
        };

        aggressiveRouteOutro.nodeVisited = (x) =>
        {
            GameplayManager.difficulty = 0.75f;
            aggressiveRouteOutro.nextNode[0] = enemyTurn[x];
        };

        for (int i = 0; i < rangoAtaques; i++)
        {
            for (int j = 0; j < insultList.replicas.Length; j++)
            {
                enemyTurn[i].nextNode[j] = nodoEnemyCorrect;
                enemyTurn[i].nextNode[i] = nodoPlayerCorrect;
            }
        }
        //Si se llega al nodoEnemyCorrect, el enemigo obtiene un punto, y el siguiente nodo será un nuevo ataque aleatorio; 
        //a menos que activateFinal haya sido activado.
        nodoEnemyCorrect.nodeVisited = (x) => {
            nodoEnemyCorrect.nextNode[0] = enemyTurn[x];
            GameplayManager.enemyScore++;
            sf.ComprobeFinal(GameplayManager.enemyScore, nodoEnemyCorrect,nodoFinEnemy);
        };
        nodoPlayerCorrect.nextNode[0] = playerTurn;
        //Lo mismo ocurre para nodoPlayerCorrect.
        
        nodoPlayerCorrect.nodeVisited = (x) => {
            GameplayManager.playerScore++;
            sf.ComprobeFinal(GameplayManager.playerScore, nodoPlayerCorrect, nodoFinPlayer);
        };

        enemyAnswerIncorrect.nextNode[0] = playerTurn;
        //Cuando el nodo playerTurn sea visitado debemos generar la respuesta de la IA y actuar en consecuencia.
        playerTurn.nodeVisited = (x) =>
        {
            for (int i = 0; i < playerTurn.answers.Length; i++)
            {
                //Respuesta correcta
                string correctHistory = insultList.replicas[i];
                //Array de strings de replicas incorrectas
                string[] otherOptions = new string[insultList.ataques.Length - 1];
                int correctIndex = System.Array.IndexOf(insultList.replicas, correctHistory);
                //Quitamos la respuesta correcta para generar correctamente todas las respuestas incorrectas
                otherOptions = insultList.replicas.Where((val, idx) => idx != correctIndex).ToArray();

                enemyAnswerCorrect[i].history = correctHistory;
                enemyAnswerIncorrect.history = otherOptions[Random.Range(0, otherOptions.Length)];

                bool isCorrect = gm.ComprobeCorrectAnswer();
                if (isCorrect)
                    playerTurn.nextNode[i] = enemyAnswerCorrect[i];
                else
                    playerTurn.nextNode[i] = enemyAnswerIncorrect;
            }
        };

        for (int i = 0; i < enemyAnswerCorrect.Length; i++)
        {
            enemyAnswerCorrect[i].nodeVisited = (x) =>
            {
                GameplayManager.enemyScore++;
                for (int j = 0; j < enemyAnswerCorrect.Length; j++)
                {
                    enemyAnswerCorrect[j].nextNode[0] = enemyTurn[x];
                    sf.ComprobeFinal(GameplayManager.enemyScore, enemyAnswerCorrect[j], nodoFinEnemy);
                }
                
            };

        }
        enemyAnswerIncorrect.nodeVisited = (x) =>
        {
            GameplayManager.playerScore++;
            sf.ComprobeFinal(GameplayManager.playerScore, enemyAnswerIncorrect, nodoFinPlayer);
        };

        nodoFinEnemy.newRound = true;
        nodoFinPlayer.newRound = true;
        nodoFinEnemy.isFinal = true;
        nodoFinPlayer.isFinal = true;

        return root;
    }


    /// <summary>
    /// Función para comprobar si se ha llegado a la puntuación final; y en ese caso, asignar el nodo final. 
    /// </summary>
    /// <param name="score">Puntuación que comprobar: Enemigo o Jugador.</param>
    /// <param name="currentNode">Nodo que llama a la función ya la que se le asigna el nodo final como nodo siguiente.</param>
    /// <param name="endingNode">Nodo final al que pasar si se cumple la puntuación máxima.</param>
    private void ComprobeFinal(int score, GameplayManager.StoryNode currentNode, GameplayManager.StoryNode endingNode)
    {
        if (score >= GameplayManager.maxScore)
            currentNode.nextNode[0] = endingNode;
    }
 
}
