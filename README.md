Proyecto realizado por Jaime Martínez Villar
-----------------------------------------------------
*						TUTORIAL DEL JUEGO					
-----------------------------------------------------
Este juego es un pequeño duelo cuya única arma es la palabra. Para cada frase o insulto
existe una única respuesta correcta.

El juego se inicia en el Menú Principal, en la que seleccionamos una Nueva Partida.

La zona de juego se separa en la "Historia" y las "Respuestas".
En la zona "Historia" aparecerá el texto descriptivo, los ataques (insultos) de la IA, así como
nuestras respuestas, una vez la hayamos seleccionado.

En la zona "Respuestas" se hallarán los posibles insultos, ataques o respuestas, de las que disponemos.

Para seleccionar una respuesta, simplemente haremos clic sobre ella. El botón correspondiente a la 
respuesta se ilumina cuando el cursor esté encima.

Tanto la IA como el juador empiezan con tres (3) puntos de vida, que se muestran en las esquinas inferiores
de la "Sección Historia", representadas por 3 calaveras. 
Si la respuesta al insulto es correcta, el enemigo perderá una vida, y además obtendremos el turno; por lo
que nos tocará lanzar un insulto a la IA; a lo cuál la IA responderá con una de las posibles respuestas.
La IA recuperará el turno, y el jugador perderá una vida, si responde correctamente. 

En cuanto el jugador o la IA se queden sin vidas se acaba el juego.

En cualquier momento podemos usar 'Esc' para abrir el menú de pausa, con el que podemos volver al "Menú
Principal", "Reiniciar la Partida", o volver a la partida, volviendo a pulsar 'Esc'.



---------------------------------------------------------
*						EXPLICACIÓN DEL CÓDIGO					
---------------------------------------------------------
Este juego se divide en tres escenas: La Pantalla Inicial, una pantalla narrativa y la pantalla de Juego.

En la Scene "Pantalla Inicial" tenemos dos botones: "Nueva Partida", que se encargará de cargar la siguiente Scene; y "Salir",
para salir de la aplicación.

El cambio de escena se ejecuta a través del Script "LevelChanger"; el cual se encarga de lanzar un Trigger (EndScene) de la animación
de transición de escena; un Fade Out a negro. Una vez completa la transición se carga la escena especificada.

La siguiente Scene es una pequeña pantalla de Introducción ("Intro Narrativa"); simplemente por dar algo de contexto. 

Esta escena está controlada por el Script "ReadFromText", el cual debe cargar un archivo de texto (.txt) de la carpeta Resources
y mostrarlo en pantalla a través de una corrutina que simula la escritura del texto; de manera que se muestra lentamente.
En la función Update de este script detectamos si el jugador pulsa cualquier botón; en cuyo caso aceleramos la velocidad a la que
aparece el texto.

Una vez el texto haya aparecido por completo mostramos el botón para avanzar escena; el cuál ejecutará la transición de escena al igual
que el botón de "Nueva Partida" de la Scene anterior; sólo que esta vez cargará la Scene "Partida Nueva".

En la Scene "Partida Nueva" es donde se desarrollael juego principalmente. El Script "GameplayManager" será el encargado de controlar
el flujo de juego, mientras que el Script "StoryFiller", el cual llamará GameplayManager al inicio, rellenará los nodos de la historia.

-"GameplayManager"-
Este nodo describe las clases que se usarán y controla el flujo del juego.
Las clases principales son:
	"Insults" - Con dos arrays de strings que representan los Ataques (o insultos) y las Replicas (o respuestas) que el jugado y la IA
	pueden ejecutar.
	
	"StoryNode" - Contiene la información necesaria del nodo:
		- hitory (string): Texto que se muestra en la Sección Historia.
		- answers (string[]): Todas las posibles respuestas que  aparecerá como botones en la Sección Respuestas.
		- nextNode (StoryNode[]): Siguientes nodos que conectan con este a través de las respuestas (answers). Debe tener el mismo número
			de nextNode que de answers.
		- isFinal (bool): Indica que es un nodo final; sólo estará en los nodos finales que determinan el vencedor.
		- newRound (bool): Indica que es una nueva ronda; por temas de diseño se ha decidido pescindir de una ventana de historia con scroll,
			y en vez de eso, limpiar la escena cada nueva ronda.
		- NodeVisited(int index) (delegate void) y nodeVisited (NodeVisited): función delegada que se ejecutará cuando se llegue al nodo;
			la utilizaremos para subir la puntuación y ejecutar ataques/respuestas aleatorias por parte de la IA.

Ahora se explicará el resto del flujo de funcinamiento del script:
	"Start()" - Al ejecutarse el script resetearemos la escena: Dificultad (difficulty), puntuaciones (enemyScore, playerScore), y destruimos
		las barras de vida; para que no aparezcan durante los nodos narrativos. Limpiamos el texto de la escena y asignamos el nodo actual
		("current) ejecutando el Script "StoryFiller". Finalmente rellenamos el UI con la función "FillUI".
		
	"FillUI()" - Rellenamos todo el UI: texto, botones, etc. Para ello empezamos viendo si ya se ha asginado una dificultad (por lo que habrá
		empezado el duelo), y entonces creamos las barras de vida. Posteriormente analizamos si es una nueva ronda (current.newRound) para 
		limpiar el texto o simplemente añadir una línea. 
		Ahora pasamos el texto (current.history) del nodo a la variable "message" y ejecutamos la corrutina "TypeText()" para mostrar dicho
		texto lentamente. Para terminar, destruimos los botones que estaban creados y creamos los nuevos, Instanciandolos como copia del
		Prefab y colocandolos según se especifíca. En mi caso los he colocado como en el juego original, un opción por línea y vamos
		desplazando con una "ScrollBar" para seleccionar el que se quiera.
		
	"TypeText()" - Corrutina para mostrar el texto lentamente; al igual que en "ReadFromText".
	
	"FillListener()" - Añade la función que se ejecuta al pulsar cada botón; se llama a través de "FillUI()".
	
	"AnswerSelected(int index)" - Función que se ejecuta al seleccionar una respuesta (pulsar un botón). Para empezar, si el texto no se
		acababa de mostrar lo aceleramos y guardamos la selección; en caso contrario, analizamos si es un nodo final (current.isFinal) para
		dar por  concluido el juego, pero si no lo es, vemos si dispone de función NodeVisited(), de tenerla, crearemos un index aleatorio, 
		para el caso en el que haya que saltar a un nuevo nodo de ataque de la IA, y ejecutamos la función. Después actualizamos el nodo
		(current = current.nextNode[index]).
		
	"ComprobeCorrectAnswer()" - Función que se llamará cuando la IA tenga que responder a nuestro insulto. La respuesta será aleatoria
		pero con un rango de acierto según la dificultad (difficulty), de manera que obtendremos un número aleatorio del 0.0 al 1.0 (0-100)
		y si es menor que la dificultad, la IA acertará, y si no, la IA responderá con cualquier otra respuesta.
		
	"Update()" - Como en la Scene anterior, se encargará de detectar cualquier input y en caso de hacerlo, acelerar el texto a mostrar.
	
	"CreateLifeBars()" - Función que crea las barras de vida una vez se haya iniciado el duelo; para ello, instancia dos copias (Jugador y 
		Enemigo) del Prefab asignado; se colocan en el lugar deseado y se asignan las imagenes de la vida a las variables "playerHealth" y
		"enemyHealth".
		
	"UpdateHealth()" - Función que actualizará visualmente el estado de las barras de vida; borrando la imagen correspondiente al punto
		obtenido.
	
	"LoadResourceTextfile(string path)" - Función auxiliar para cargar un TextAsset a partir de un archivo de texto (.txt, .json, etc.),
		y devolver el texto como cadena. Desafortunadamente, no conseguí que funcionase.
		

- "StoryFiller" -
Este script crea los nodos de la historia a través de la función CreateNode(string history, string[] answers), dandole un texto para mostrar
(history) y un array de strings que serán las opciones/botones de los que dispondrá el jugador en ese nodo.

Para almacenar los posibles insultos y respuestas utilizamos un archivo .json configurado como la clase "Insults" en "GameplayManager", el cual
dispone de dos arrays de strings; correspondientes a los "ataques" (o insultos) y las "replicas" (o respuestas).

Los primeros nodos que crearemos son fijos, simplemente para poner en situación al jugador; después tenemos los "enemyTurn", un array de nodos
que corresponden a los diferentes ataques que puede llevar a cabo la IA; las respuestas aquí son todas las réplicas disponibles.

Luego tenemos el nodo "playerTurn", en el cual dispondremos de todos los posibles ataques.
También tenemos nodos de conexión, simplemente para determinar quién ha ganado la ronda, y los nodos finales, para determinar el ganador de la
partida.

En la parte siguiente se relacionan los nodos unos con otros; y se añaden las funciones NodeVisited:
	
	pacificRouteOutro.nodeVisited - Asigna la dificultad a que la IA acierta 2 de cada 3 (difficulty = 0.6) y asigna el siguiente nodo a 
		un turno aleatorio del enemigo.
		
	aggressiveRouteOutro.nodeVisited - Al igual que el anterior pero asigna la dificultad a 0.75.
	
Con esto los nodos narrativos sí sirven para algo, aunque el jugador no lo note; pues dependiendo de si empieza de forma más agresiva o 
pacífica, el juego será más fácil o difícil.

	nodeEnemyCorrect.nodeVisited - Llegaremos a este nodo tras el turno del enemigo si fallamos; por lo cual, aumentamos la puntuación del
		enemigo, seleccionamos el siguiente nodo como un enemyTurn[x] (aleatorio) y antes de retornar, ejecutamos la función "ComprobeFinal",
		la cuál analizará si se ha obtenido la puntuación máxima, y en ese caso, asignar como nodo siguiente el nodo final.
		
	nodePlayerCorrect.nodeVisited - Al igual que el anterior, pero para puntuar al jugador. Además el próximo nodo por defecto, de no haber
		alcanzado la puntuación final será playerTurn.
		
	enemyAnswerCorrect.nodeVisited - Cuando la IA nos responda correctamente. El funcionamiento de este delegado será igual que en 
		enemyCorrect.nodeVisited.
	
	enemyAnswerIncorrect.nodeVisited - Cuando la IA responda de forma incorrecta. Su funcionamiento es el mismo que nodePlayerCorrect.nodeVisited.
	
En estas funciones delegadas se actualizaran las puntuaciones y es donde estará el peso principal del juego; el flujo de nodos.

	playerTurn.nodeVisited - Este delegado es el más extenso; puesto que cada vez que sea el turno del jugado se debe asignar a cada ataque
		de este, una respuesta aleatoria de la IA con una probabilidad de aceirto según "difficulty". 
		Para cada ataque del jugador asignamos una respuesta; que en caso de ser incorrecta debemos sustraer la correcta del array y seleccionar
		una aleatoria ("otherOptions = insultList.replicas.Where((val, idx) => idx != correctIndex).ToArray()")
		
Una vez alcanzado el nodo final; ya sea con victoria del jugador o de la IA, el juego acaba. La única forma de continuar será pulsar "Esc" para
abrir el menú de pausa y seleccionar "Reiniciar Partida" para volver a jugar desde la Scene "Partida Nueva", o volver a la "Pantalla Inicial" 
pulsando sobre "Menú Principal". 

------------------------------------------------------------------------------
*							NOTA					
------------------------------------------------------------------------------

En el proyecto se han utilizado imagenes y música con copyright puesto que su uso es simplemente a modo de muestra y sin animo de lucro.
Dichas imagenes y audios serían reemplazados en un producto final.




 



